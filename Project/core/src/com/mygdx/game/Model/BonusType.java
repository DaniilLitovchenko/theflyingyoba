package com.mygdx.game.Model;

/**
 * Created by USER on 22.02.2016.
 */
public enum BonusType {
    EXTRALIFE,
    SPEEDUP,
    SPEEDDOWN
}
