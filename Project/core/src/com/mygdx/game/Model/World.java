package com.mygdx.game.Model;

/**
 * Created by USER on 15.02.2016.
 */


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.mygdx.game.MyGdxGame;
import com.mygdx.game.Screens.PlayScreen;
import com.mygdx.game.View.AnimatedActor;
import com.mygdx.game.View.ImageActor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

public class World extends Stage {
    private ImageActor background;
    private Player player;
    private Random r;
    private ArrayList<Meteor> meteors;
    private float SPEED_MULT = 2.5f;
    Timer timer;
    public World(ScreenViewport screenViewport, SpriteBatch batch, HashMap<String, TextureRegion> textureRegions) {
        super(screenViewport, batch);
        r = new Random();
        meteors = new ArrayList<Meteor>();
        background = new ImageActor(textureRegions.get("playground"), 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        addActor(background);
        player = new Player(textureRegions.get(String.format("skin%d", r.nextInt(7) + 1)), Gdx.graphics.getWidth() / 2, 0, 80 * SPEED_MULT, 0.5f);
        addActor(player);

        AnimatedActor animatedActor = new AnimatedActor(new TextureRegion[] {
                textureRegions.get("frame1"),
                textureRegions.get("frame2"),
                textureRegions.get("frame3"),
                textureRegions.get("frame4")}, 100, 100, 50, 50, 2.0f);
        addActor(animatedActor);

        timer = new Timer();
        timer.scheduleTask(new Timer.Task() {
            @Override
            public void run() {
                Meteor m = new Meteor(
                        MyGdxGame.getInstance().getTextureRegions().get(String.format("meteor%d", r.nextInt(12) + 1)),
                        r.nextInt(Gdx.graphics.getWidth()), Gdx.graphics.getHeight(), 0.25f, 0, -30, r.nextInt() % 10);
                addActor(m);
                meteors.add(m);
            }
        }, 1, 4);
        timer.scheduleTask(new Timer.Task() {
            @Override
            public void run() {
                int score = 0;
                score += 3;
            }
        }, 1, 1);
        timer.start();


    }


    public Player getPlayer() {
        return player;
    }

    public void act (float delta) {
        super.act(delta);
        for(Meteor m : meteors)
            if (m.getCircle().overlaps(player.getCircle())) {
                resume();
                MyGdxGame.getInstance().showMainMenuScreen();
            }
    }
    public void resume(){
        for (Meteor m : meteors
             ) {
            m.remove();
        }
    }
}


