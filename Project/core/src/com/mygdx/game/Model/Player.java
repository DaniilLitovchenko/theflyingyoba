package com.mygdx.game.Model;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Circle;
import com.mygdx.game.Controller.Direction;
import com.mygdx.game.View.ImageActor;


public class Player extends ImageActor {
    private Direction direction;
    private Circle circle;

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    private float step;
    private float scale;

    public Player(TextureRegion img, float x, float y, float step, float scale) {
        super(img, x, y);
        direction = Direction.NONE;
        this.step = step;
        this.scale = scale; //масштаб
    }


    public void act(float delta) {
        switch (direction) {
            case LEFT:
                moveBy(-step * delta, 0);
                if (getX() < 0)
                    setX(0);
                break;
            case RIGHT:
                moveBy(step * delta, 0);
                if (getX() + getWidth()*0.5 > Gdx.graphics.getWidth())
                    setX(Gdx.graphics.getWidth() - getWidth()/2);
                break;
        }
    }


    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(img, getX(), getY(), getOriginX(), getOriginY(), getWidth(), getHeight(), scale, scale, getRotation());
        batch.end();
        ShapeRenderer shape = new ShapeRenderer();
        shape.setProjectionMatrix(batch.getProjectionMatrix());
        shape.begin(ShapeRenderer.ShapeType.Line);
        shape.circle(getX() + getWidth()*scale / 2, getY() + (getHeight() - getWidth() / 2)*scale, getWidth()*scale / 2);
        shape.end();
        batch.begin();
    }

    public Circle getCircle() {
        return new Circle(getX() + getWidth()*scale / 2, getY() + (getHeight() - getWidth() / 2)*scale, getWidth()*scale / 2);
    }
}

