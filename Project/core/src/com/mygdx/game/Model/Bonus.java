package com.mygdx.game.Model;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.mygdx.game.View.ImageActor;

/**
 * Created by USER on 22.02.2016.
 */
public class Bonus extends ImageActor {
    private BonusType type;
    private int fallingSpeed;

    public Bonus(TextureRegion img, float x, float y, float width, float height) {
        super(img, x, y, width, height);
    }

    public Bonus(Texture img, float x, float y) {
        super(img, x, y);
    }

    public Bonus(Texture img, float x, float y, float width, float height) {
        super(img, x, y, width, height);
    }

    public Bonus(TextureRegion img, float x, float y) {
        super(img, x, y);
    }
    public void setBonusType(BonusType type){
        this.type = type;
    }
    public void setFallingSpeed(int fallingSpeed) {
        this.fallingSpeed = fallingSpeed;
    }

    public void act(float delta){
        setY(getY() - fallingSpeed * delta);
        if (getY()<0)
            remove();
    }
}
