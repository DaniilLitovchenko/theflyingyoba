package com.mygdx.game.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.mygdx.game.Controller.MoveToMainMenu;
import com.mygdx.game.View.ImageActor;

import java.util.HashMap;

/**
 * Created by 04k1403 on 20.01.2016.
 */
public class SettingsScreen implements Screen {
    private Stage stage;
    private ImageActor backGround;
    private ImageActor backButton;
    public SettingsScreen(SpriteBatch batch, HashMap<String, TextureRegion> textureRegions){
        backGround=new ImageActor(new Texture("android/assets/bg.png"),0,0);
        backButton=new ImageActor(textureRegions.get("backbutton"),0,0);
        backButton.addListener(new MoveToMainMenu());

        OrthographicCamera camera=new OrthographicCamera();
        camera.setToOrtho(false, Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
        stage=new Stage(new ScreenViewport(camera), batch);

        stage.addActor(backGround);
        stage.addActor(backButton);

    }
    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(delta);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}

