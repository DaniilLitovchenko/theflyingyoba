package com.mygdx.game.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.mygdx.game.Controller.MoveToPlayScreen;
import com.mygdx.game.Controller.MoveToRatingScreen;
import com.mygdx.game.Controller.MoveToSettingsScreen;
import com.mygdx.game.View.ImageActor;

import java.util.HashMap;

/**
 * Created by USER on 20.01.2016.
 */
public class MainMenuScreen implements Screen {
    private ImageActor backGround;
    private ImageActor logo;
    private ImageActor playButton;
    private ImageActor settingButton;
    private ImageActor ratingButton;
    private Stage stage;

    public MainMenuScreen(SpriteBatch batch, HashMap<String, TextureRegion> textureRegions) {
        backGround = new ImageActor(new Texture("android/assets/bg.png"), 0, 0);
        logo = new ImageActor(textureRegions.get("logo"),50,290);
        playButton = new ImageActor(textureRegions.get("playbutton"),50,50);
        playButton.addListener(new MoveToPlayScreen());
        settingButton = new ImageActor(textureRegions.get("settingsbutton"),250,50);
        settingButton.addListener(new MoveToSettingsScreen());
        ratingButton = new ImageActor(textureRegions.get("ratingbutton"),450,50);
        ratingButton.addListener(new MoveToRatingScreen());

        OrthographicCamera camera = new OrthographicCamera();
        camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        stage = new Stage(new ScreenViewport(camera), batch);

        stage.addActor(backGround);
        stage.addActor(logo);
        stage.addActor(playButton);
        stage.addActor(settingButton);
        stage.addActor(ratingButton);
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(216 / 255f, 213 / 255f, 0f, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(delta);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
    }

}


