package com.mygdx.game.Screens;



import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.mygdx.game.Controller.PlayerController;
import com.mygdx.game.Model.World;

import java.util.HashMap;

/**
 * Created by 04k1403 on 27.01.2016.
 */
public class PlayScreen implements Screen {
    private World world;
    private PlayerController controller;
    private InputMultiplexer multiplexer;

    public PlayScreen(SpriteBatch batch, HashMap<String, TextureRegion> textureRegions) {
        OrthographicCamera camera = new OrthographicCamera();
        camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        world = new World(new ScreenViewport(camera), batch, textureRegions);
        controller = new PlayerController(world);
        multiplexer = new InputMultiplexer();
        multiplexer.addProcessor(world);
        multiplexer.addProcessor(controller);
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(multiplexer);
    }

    @Override
    public void render(float delta) {
        Gdx.gl20.glClear(GL20.GL_COLOR_BUFFER_BIT);
        controller.updatePlayer();
        world.act(delta);
        world.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
