package com.mygdx.game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.mygdx.game.Controller.ControlType;
import com.mygdx.game.Screens.MainMenuScreen;
import com.mygdx.game.Screens.PlayScreen;
import com.mygdx.game.Screens.RatingScreen;
import com.mygdx.game.Screens.SettingsScreen;

import java.util.HashMap;

public class MyGdxGame extends Game{
	private MainMenuScreen mainMenuScreen;
	private SettingsScreen settingsScreen;
	private PlayScreen playScreen;
	private RatingScreen ratingScreen;
	private SpriteBatch batch;
	private float ppuX;
	private float ppuY;
	private HashMap<String, TextureRegion> textureRegions;
	private static MyGdxGame ourInstance=new MyGdxGame();
	private ControlType controltype;

	public static MyGdxGame getInstance(){return ourInstance;}
	private MyGdxGame(){}
	@Override
	public void create () {
		controltype = ControlType.BUTTONS;
		setPpuX(Gdx.graphics.getWidth()/640f);
		setPpuY(Gdx.graphics.getHeight()/480f);
		loadGraphics();
		batch = new SpriteBatch();
		mainMenuScreen=new MainMenuScreen(batch,textureRegions);
		settingsScreen = new SettingsScreen(batch,textureRegions);
		playScreen = new PlayScreen(batch,textureRegions);
		ratingScreen=new RatingScreen(batch,textureRegions);
		showMainMenuScreen();
	}
	public void showMainMenuScreen(){
		setScreen(mainMenuScreen);
	}
	public void showSettingsScreen(){
		setScreen(settingsScreen);
	}
	public void showPlayScreen(){
		setScreen(playScreen);
	}
	public void showRatingScreen(){
		setScreen(ratingScreen);
	}
	private void loadGraphics(){
		Texture atlas1=new Texture("android/assets/atlas1.png");
		textureRegions=new HashMap<String, TextureRegion>();
		textureRegions.put("logo", new TextureRegion(atlas1,0,0,531,155));
		textureRegions.put("playbutton", new TextureRegion(atlas1,0,155,140,140));
		textureRegions.put("settingsbutton", new TextureRegion(atlas1,140,155,140,140));
		textureRegions.put("ratingbutton", new TextureRegion(atlas1,280,155,140,140));
		textureRegions.put("backbutton",new TextureRegion(atlas1,0,295,140,140));
		Texture atlas2=new Texture("android/assets/atlas2.png");
		textureRegions.put("meteor1",new TextureRegion(atlas2,0,0,243,243));
		textureRegions.put("meteor2",new TextureRegion(atlas2,243,0,243,243));
		textureRegions.put("meteor3",new TextureRegion(atlas2,486,0,243,243));
		textureRegions.put("meteor4",new TextureRegion(atlas2,729,0,243,243));
		textureRegions.put("meteor5",new TextureRegion(atlas2,972,0,243,243));
		textureRegions.put("meteor6",new TextureRegion(atlas2,1228,0,243,243));
		textureRegions.put("meteor7",new TextureRegion(atlas2,1498,0,243,243));
		textureRegions.put("meteor8",new TextureRegion(atlas2,1760,0,243,243));
		textureRegions.put("meteor9",new TextureRegion(atlas2,0,243,243,243));
		textureRegions.put("meteor10",new TextureRegion(atlas2,243,243,243,243));
		textureRegions.put("meteor11",new TextureRegion(atlas2,527,275,455,301));
		textureRegions.put("meteor12",new TextureRegion(atlas2,1032,248,279,473));
		textureRegions.put("meteor13",new TextureRegion(atlas2,1361,333,484,126));
		Texture atlas3=new Texture("android/assets/atlas3.png");
		textureRegions.put("skin1",new TextureRegion(atlas3,0,0,220,370));
		textureRegions.put("skin2",new TextureRegion(atlas3,219,0,216,355));
		textureRegions.put("skin3",new TextureRegion(atlas3,433,0,286,370));
		textureRegions.put("skin4",new TextureRegion(atlas3,718,0,228,370));
		textureRegions.put("skin5",new TextureRegion(atlas3,943,0,231,370));
		textureRegions.put("skin6",new TextureRegion(atlas3,1172,0,232,370));
		textureRegions.put("skin7",new TextureRegion(atlas3,1406,0,230,370));
		Texture playground=new Texture("android/assets/playground.png");
		textureRegions.put("playground",new TextureRegion(playground,0,0,640,480));
		Texture animationFrames = new Texture("android/assets/animationframes.png");
		textureRegions.put("frame1",new TextureRegion(animationFrames, 0, 0,50,50));
		textureRegions.put("frame2",new TextureRegion(animationFrames,50, 0,50,50));
		textureRegions.put("frame3",new TextureRegion(animationFrames, 0,50,50,50));
		textureRegions.put("frame4",new TextureRegion(animationFrames,50,50,50,50));
	}

	public float getPpuX() {
		return ppuX;
	}
	public void setPpuX(float ppuX) {
		this.ppuX = ppuX;
	}
	public float getPpuY() {
		return ppuY;
	}
	public void setPpuY(float ppuY) {
		this.ppuY = ppuY;
	}

	public HashMap<String, TextureRegion> getTextureRegions() {
		return textureRegions;
	}

	public ControlType getControltype() {
		return controltype;
	}
}


