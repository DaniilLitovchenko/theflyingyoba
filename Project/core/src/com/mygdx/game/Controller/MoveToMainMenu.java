package com.mygdx.game.Controller;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.mygdx.game.MyGdxGame;

/**
 * Created by 04k1403 on 27.01.2016.
 */
public class MoveToMainMenu extends ClickListener {
    public void clicked(InputEvent event, float x, float y) {
        MyGdxGame.getInstance().showMainMenuScreen();
    }
}
