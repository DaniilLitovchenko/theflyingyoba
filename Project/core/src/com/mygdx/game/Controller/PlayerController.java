package com.mygdx.game.Controller;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.mygdx.game.Model.World;
import com.mygdx.game.MyGdxGame;
import com.mygdx.game.Model.Player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import static com.badlogic.gdx.Input.Keys.*;

/**
 * Created by USER on 22.02.2016.
 */
public class PlayerController implements InputProcessor {
    Player player;
    HashSet<Integer> pressedKeys;
    ArrayList<Character> printedChars;
    HashMap<Integer, Pointer> touchedPointers;
    HashSet<Integer> controlledButtons;
    ControlType control;

    public PlayerController(World world) {
        player = world.getPlayer();
        pressedKeys = new HashSet<Integer>();
        printedChars = new ArrayList<Character>();
        touchedPointers = new HashMap<Integer, Pointer>();
        control = MyGdxGame.getInstance().getControltype();
        controlledButtons = new HashSet<Integer>();
        controlledButtons.add(A);
        controlledButtons.add(D);
        controlledButtons.add(LEFT);
        controlledButtons.add(RIGHT);
    }

    public void updatePlayer() {
        player.setDirection(getCurrentDirection());
    }

    public Direction getCurrentDirection() {
        switch (control) {
            case BUTTONS:
            case TOUCH:
                if (movesLeft() && !movesRight()) return Direction.LEFT;
                if (!movesLeft() && movesRight()) return Direction.RIGHT;
                break;
        }
        return Direction.NONE;
    }

    private boolean movesLeft() {
        switch (control) {
            case BUTTONS:
                return pressedKeys.contains(A) || pressedKeys.contains(LEFT);
            case TOUCH:
                if (!touchedPointers.isEmpty()) {
                    Pointer p = touchedPointers.values().iterator().next();
                    if (2 * p.getX() <= Gdx.graphics.getWidth())
                        return true;
                }
                break;
        }
        return false;
    }

    private boolean movesRight() {
        switch (control) {
            case BUTTONS:
                return pressedKeys.contains(D) || pressedKeys.contains(RIGHT);
            case TOUCH:
                if (!touchedPointers.isEmpty()) {
                    Pointer p = touchedPointers.values().iterator().next();
                    if (2 * p.getX() > Gdx.graphics.getWidth())
                        return true;
                }
                break;
        }
        return false;
    }


    @Override
    public boolean keyDown(int keycode) {
        if (controlledButtons.contains(keycode))
            return pressedKeys.add(keycode);
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return controlledButtons.contains(keycode) && pressedKeys.remove(keycode);
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        if (!touchedPointers.containsKey(pointer)) {
            touchedPointers.put(pointer, new Pointer(screenX, screenY, button));
            return true;
        }
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        if (!touchedPointers.containsKey(pointer)) {
            touchedPointers.remove(pointer);
            return true;
        }
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        if (touchedPointers.containsKey(pointer)) {
            touchedPointers.get(pointer).setCoords(screenX, screenY);
            return true;
        }
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}


