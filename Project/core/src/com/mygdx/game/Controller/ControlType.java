package com.mygdx.game.Controller;

/**
 * Created by USER on 22.02.2016.
 */
public enum ControlType {
    TILT,
    BUTTONS,
    TOUCH,
    NONE
}
