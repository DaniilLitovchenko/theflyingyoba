package com.mygdx.game.View;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Created by Гриша on 28.06.2016.
 */
public class AnimatedActor extends Actor {

    private Animation animation;
    private float animationDuration;
    private float stateTime;

    public AnimatedActor(TextureRegion[] frames, float x, float y, float width, float height, float animationDuration) {
        this.animationDuration = animationDuration;
        this.stateTime = 0;
        this.animation = new Animation(animationDuration / frames.length, frames);
        setPosition(x * com.mygdx.game.MyGdxGame.getInstance().getPpuX(), y * com.mygdx.game.MyGdxGame.getInstance().getPpuY());
        setSize(width * com.mygdx.game.MyGdxGame.getInstance().getPpuX(), height * com.mygdx.game.MyGdxGame.getInstance().getPpuY());
    }

    public void act(float delta) {
        stateTime += delta;
        stateTime %= animationDuration;
    }

    public void draw(Batch batch, float parentAlpha) {
        batch.draw(animation.getKeyFrame(stateTime), getX(), getY(), getWidth(), getHeight());
    }
}
