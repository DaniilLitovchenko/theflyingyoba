package com.mygdx.game.View;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Created by 04k1403 on 27.01.2016.
 */
public class ImageActor extends Actor {
    protected TextureRegion img;

    public ImageActor(TextureRegion img, float x, float y, float width, float height) {
        this.img = img;
        setPosition(x * com.mygdx.game.MyGdxGame.getInstance().getPpuX(), y * com.mygdx.game.MyGdxGame.getInstance().getPpuY());
        setSize(width * com.mygdx.game.MyGdxGame.getInstance().getPpuX(), height * com.mygdx.game.MyGdxGame.getInstance().getPpuY());
    }

    public ImageActor(Texture img, float x, float y, float width, float height) {
        this(new TextureRegion(img), x, y, width, height);
    }

    public ImageActor(Texture img, float x, float y) {
        this(new TextureRegion(img), x, y, img.getWidth(), img.getHeight());
    }

    public ImageActor(TextureRegion img, float x, float y) {
        this(img, x, y, img.getRegionWidth(), img.getRegionHeight());
    }

    public void draw(Batch batch, float parentAlpha) {
        if (img != null)
        batch.draw(img, getX(), getY(), getWidth(), getHeight());
    }
}
